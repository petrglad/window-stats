package window_stats;

import com.google.common.collect.Lists;
import org.junit.Test;

import static org.junit.Assert.*;

public class StatsTest {

    @Test
    public void testSecondsToBucket0() {
        Stats stats = new Stats(1, () -> 20L * 1000);
        assertTrue(0 > stats.secondToBucket(10));
        assertTrue(-1 == stats.secondToBucket(19));
        assertEquals(0, stats.secondToBucket(20));
    }

    @Test
    public void testSecondsToBucket1() {
        Stats stats = new Stats(10, () -> 20L * 1000);
        assertTrue(0 > stats.secondToBucket(10));
        assertEquals(0, stats.secondToBucket(11));
        assertEquals(4, stats.secondToBucket(15));
        assertEquals(9, stats.secondToBucket(20));

        // Unexpected case, ensuring it still won't break.
        assertEquals(10, stats.secondToBucket(21));
    }

    @Test
    public void testMillisToSecond() {
        assertEquals(1234L, Stats.millisToSecond(1234567L));
    }

    @Test
    public void testIsEmpty() {
        Stats.State s = new Stats.State();
        assertTrue(s.isEmpty());
        assertFalse(s.add(123456.0).isEmpty());
    }

    @Test
    public void testIsEmpty2() {
        assertTrue(Stats.State.combine(Lists.newArrayList()).isEmpty());

        Stats.State emptyState = new Stats.State();
        assertTrue(Stats.State.combine(Lists.newArrayList(emptyState)).isEmpty());

        Stats.State state = new Stats.State(0.0, 1.0, 2.0, 3);
        assertFalse(Stats.State.combine(Lists.newArrayList(state)).isEmpty());
    }

    @Test
    public void testSateCombine() {
        Stats.State combo = Stats.State.combine(Lists.newArrayList(
                new Stats.State().add(7.0).add(5.0),
                new Stats.State().add(5.0),
                new Stats.State().add(3.0)));
        assertEquals(4, combo.count);
        assertEquals(20.0, combo.sum, 1e-20);
        assertEquals(7.0, combo.max, 1e-20);
        assertEquals(3.0, combo.min, 1e-20);
        assertEquals(5.0, combo.getAvg(), 1e-20);
    }

    @Test
    public void testFutureTx() {
        Stats stats = new Stats(10, () -> 20L * 1000);
        assertFalse(stats.update(new Transaction(21000L, 12.3)));
        Stats.State state = stats.get();
        assertTrue(state.isEmpty());
    }

    @Test
    public void testSingleTx() {
        Stats stats = new Stats(10, () -> 20L * 1000);
        assertTrue(stats.update(new Transaction(15000L, 12.3)));
        assertFalse(stats.update(new Transaction(1000L, 32.1)));
        Stats.State state = stats.get();
        assertEquals(1, state.count);
        assertEquals(12.3, state.sum, 1e-20);
        assertEquals(12.3, state.max, 1e-20);
        assertEquals(12.3, state.min, 1e-20);
        assertEquals(12.3, state.getAvg(), 1e-20);
    }

    @Test
    public void test2Tx() {
        Stats stats = new Stats(10, () -> 20L * 1000);
        assertTrue(stats.update(new Transaction(15000L, 60.0)));
        assertTrue(stats.update(new Transaction(17000L, 50.0)));
        Stats.State state = stats.get();
        assertEquals(2, state.count);
        assertEquals(110.0, state.sum, 1e-20);
        assertEquals(60.0, state.max, 1e-20);
        assertEquals(50.0, state.min, 1e-20);
        assertEquals(55.0, state.getAvg(), 1e-20);
    }

    @Test
    public void test3Tx() {
        Stats stats = new Stats(10, () -> 20L * 1000);
        assertTrue(stats.update(new Transaction(15000L, 60.0)));
        assertTrue(stats.update(new Transaction(15000L, 60.0)));
        assertTrue(stats.update(new Transaction(17000L, 50.0)));
        Stats.State state = stats.get();
        assertEquals(3, state.count);
        assertEquals(170.0, state.sum, 1e-20);
        assertEquals(60.0, state.max, 1e-20);
        assertEquals(50.0, state.min, 1e-20);
        assertEquals(56.6666666, state.getAvg(), 1e-6);
    }

    @Test
    public void testExpiration() {
        final long[] now = new long[]{20L * 1000};
        final Stats stats = new Stats(10, () -> now[0]);
        assertTrue(stats.update(new Transaction(15000L, 60.0)));
        {
            Stats.State state = stats.get();
            assertEquals(1, state.count);
            assertEquals(60.0, state.sum, 1e-20);
        }
        assertTrue(stats.update(new Transaction(17000L, 50.0)));
        {
            Stats.State state = stats.get();
            assertEquals(2, state.count);
            assertEquals(110.0, state.sum, 1e-20);
        }
        now[0] = 26000L;
        {
            Stats.State state = stats.get();
            assertEquals(1, state.count);
            assertEquals(50.0, state.sum, 1e-20);
        }
        now[0] = 30000L;
        {
            Stats.State state = stats.get();
            assertEquals(0, state.count);
            assertEquals(0.0, state.sum, 1e-20);
        }
    }

    @Test
    public void testLongExpiration() {
        final long[] now = new long[]{20L * 1000};
        final Stats stats = new Stats(10, () -> now[0]);
        assertTrue(stats.get().isEmpty());
        assertTrue(stats.update(new Transaction(15000L, 60.0)));
        assertFalse(stats.get().isEmpty());
        assertTrue(stats.update(new Transaction(17000L, 50.0)));
        {
            Stats.State state = stats.get();
            assertEquals(2, state.count);
            assertEquals(110.0, state.sum, 1e-20);
        }
        now[0] = 1000000L; // Whole buffer should be invalidated.
        {
            Stats.State state = stats.get();
            assertEquals(0, state.count);
            assertEquals(0.0, state.sum, 1e-20);
            assertTrue(state.isEmpty());
        }
    }
}
