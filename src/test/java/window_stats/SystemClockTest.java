package window_stats;

import org.junit.Test;

import static org.junit.Assert.*;

public class SystemClockTest {

    @Test
    public void get() {
        assertTrue(50 > System.currentTimeMillis() - new SystemClock().get());
    }
}
