package window_stats;

import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.*;

public class ApiControllerTest {

    @Test
    public void statistics() {
        final ApiController ctl = new ApiController();
        ctl.stats = new Stats(10, () -> 4321L);
        assertEquals(
                ImmutableMap.of(
                        "sum", 0.0,
                        "avg", 0.0,
                        "max", 0.0,
                        "min", 0.0,
                        "count", 0),
                ctl.statistics());
    }

    @Test
    public void registerTransaction() {
        final ApiController ctl = new ApiController();
        ctl.stats = new Stats(10, () -> 10000000L);
        assertEquals(204,
                ctl.registerTransaction(new Transaction(0L, 44.0))
                        .getStatusCodeValue());
        assertEquals(201,
                ctl.registerTransaction(new Transaction(10000000L, 44.0))
                        .getStatusCodeValue());
    }
}
