package window_stats;

import org.springframework.stereotype.Component;

import java.util.function.Supplier;

@Component
public class SystemClock implements Supplier<Long> {
    @Override
    public Long get() {
        return System.currentTimeMillis();
    }
}
