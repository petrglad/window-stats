package window_stats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

/**
 * Transaction statistics for sliding time window .
 *
 * @see Transaction
 */
@Component
public class Stats {
    private final static Logger logger = LoggerFactory.getLogger(Stats.class);

    /**
     * Snapshot of stats values.
     */
    static public class State {
        final double sum;
        final double max;
        final double min;
        final int count;

        static final State EMPTY = new State();

        State() {
            this.sum = 0;
            this.max = Double.MIN_VALUE;
            this.min = Double.MAX_VALUE;
            this.count = 0;
        }

        State(double sum, double max, double min, int count) {
            this.sum = sum;
            this.max = max;
            this.min = min;
            this.count = count;
        }

        public boolean isEmpty() {
            return this.count == 0;
        }

        Double getAvg() {
            if (count == 0) {
                return 0.0;
            } else {
                return sum / count;
            }
        }

        State add(double x) {
            return new State(
                    sum + x,
                    Math.max(max, x),
                    Math.min(min, x),
                    count + 1);
        }

        static State combine(Iterable<State> values) {
            double sum = 0;
            double max = Double.MIN_VALUE;
            double min = Double.MAX_VALUE;
            int count = 0;
            for (State v : values) {
                sum += v.sum;
                max = Math.max(max, v.max);
                min = Math.min(min, v.min);
                count += v.count;
            }
            return new State(sum, max, min, count);
        }
    }

    private List<State> buckets;
    private final Supplier<Long> clock;
    private final int windowSeconds;
    private long currentSecond;
    private State state;

    @Autowired
    public Stats(@Value("#{statsTimeWindowSeconds}") int windowSeconds,
                 Supplier<Long> clock) {
        assert windowSeconds > 0;
        this.clock = clock;
        this.windowSeconds = windowSeconds;
        resetBuckets(clockSecond());
    }

    private void resetBuckets(long second) {
        buckets = new ArrayList<>(windowSeconds);
        while (buckets.size() < windowSeconds) {
            addBucket();
        }
        state = State.EMPTY;
        currentSecond = second;
    }

    /**
     * Get latest stats.
     */
    public synchronized State get() {
        // Performance: can use read/write lock instead of synchronized if bucket list is up-to date we can just return value
        shiftBuffer();
        return state;
    }

    /**
     * Record this transaction.
     *
     * @return true - transaction is accounted for; false - the transaction is outdated and was ignored.
     */
    public synchronized boolean update(Transaction tx) {
        shiftBuffer();
        long txSecond = millisToSecond(tx.timestamp);
        int bucket = secondToBucket(txSecond);
        if (bucket < 0)
            return false;
        if (bucket >= buckets.size()) {
            // It is possible to extend the buffer somewhat into future, ignoring it for now.
            logger.warn("Transaction is in the future, ignoring. now={}, transaction.timestamp={}", clock.get(), tx.timestamp);
            return false;
        }
        final State prevBucketState = buckets.get(bucket);
        buckets.set(bucket, prevBucketState.add(tx.amount));
        state = state.add(tx.amount);
        return true;
    }

    int secondToBucket(long txSecond) {
        return buckets.size() - 1 - (int) (currentSecond - txSecond);
    }

    /**
     * Drop outdated values from history.
     * Ensures that current second matches clock's one.
     */
    private void shiftBuffer() {
        long now = clockSecond();
        int shift = (int) (now - currentSecond);
        if (shift > buckets.size()) {
            resetBuckets(now); // Optimization: Whole buffer is outdated.
        } else if (shift > 0) {
            buckets.subList(0, Math.min(buckets.size(), shift)).clear();
            for (int i = 0; i < shift; i++) {
                addBucket();
            }
            assert buckets.size() == windowSeconds;
            state = State.combine(buckets);
            currentSecond = now;
        } // else no update necessary
    }

    /**
     * Append a new empty bucket.
     */
    private void addBucket() {
        buckets.add(State.EMPTY);
    }

    static long millisToSecond(long timestampMillis) {
        return TimeUnit.MILLISECONDS.toSeconds(timestampMillis);
    }

    private long clockSecond() {
        return millisToSecond(clock.get());
    }
}
