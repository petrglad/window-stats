package window_stats;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class ApiController {
    private final static Logger logger = LoggerFactory.getLogger(ApiController.class);

    @Autowired
    Stats stats;

    @RequestMapping("/")
    public String index() {
        return "Endpoints:\n  GET /statistics\n  POST /transactions";
    }


    @RequestMapping("/statistics")
    public Map<String, Number> statistics() {
        Assert.notNull(stats, "The 'stats' field is not initialized.");
        final Stats.State state = stats.get();
        final Map<String, Number> result = new HashMap<>();
        result.put("sum", state.sum);
        result.put("avg", state.getAvg());
        result.put("max", state.isEmpty() ? 0 : state.max);
        result.put("min", state.isEmpty() ? 0 : state.min);
        result.put("count", state.count);
        return result;
    }

    @RequestMapping(value = "/transactions", method = POST)
    public ResponseEntity<String> registerTransaction(@RequestBody Transaction tx) {
        // The these status codes are required by API specification.
        if (stats.update(tx)) {
            return new ResponseEntity<>(HttpStatus.CREATED); // OK
        } else {
            logger.info("Ignored transaction outside time window. transaction.timestamp={}", tx.timestamp);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT); // Transaction is too old or in future
        }
    }
}


