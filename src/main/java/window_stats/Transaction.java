package window_stats;

import com.fasterxml.jackson.annotation.JsonProperty;

class Transaction {
    public final Long timestamp;
    public final Double amount;

    public Transaction(@JsonProperty("timestamp") Long timestamp,
                       @JsonProperty("amount") Double amount) {
        this.amount = amount;
        this.timestamp = timestamp;
    }
}
