# Time window statistics


## Build and run

Gradle of version at least 4.0 is required.

To build and run the service launch `runme.sh`.

To probe the API manually you can use following:
```
curl http://localhost:8080/statistics
```
or 
```
curl -X POST -H "Content-Type: application/json" \
    http://localhost:8080/transactions \
    -d "{\"amount\":12.3, \"timestamp\": $(date +%s)000}"
```


## Implementation notes
 
Using approximate algorithm with second long buckets that 
are dropped when expired. This helps to keep memory usage 
limited when processing large number of transactions. 
It is possible to make bucket density a parameter so 
one can adjust time precision if necessary.  

It is not specified how to handle future transactions. 
It is possible to see them due to clocks skew. 
The implementation ignores them, but is it possible 
to handle this case e.g. by providing buckets 
for several seconds in future. 
Alternatively one may use a clock that intentionally lags a little. 
This would look like a hack but will do nonetheless.

Using reasonable defaults if there are no transactions.
Assuming users will observe "count" property to tell if that is the case.

For a real project I'd considered something like 
https://github.com/dropwizard/metrics first.


## Performance

There are several ways to improve performance if necessary.
I would not do them unless there's some way to test/monitor 
actual effect of these changes. This is because they 
complicate the code further.

1. Use circular (ring) buffer for Stats buckets. 
Currently using ArrayList that copies it's tail when element is removed.

2. Use mutable Stats.State to avoid allocating unnecessary objects.

3. Use more precise locks e.g. read/write lock that would not block
several concurrent readers. Also updates to distinct buckets 
can be done in parallel.
